*** Settings ***
Documentation  This is a test for cheking negative login test of website http://rozetka.com.ua/
Library  Selenium2Library
Resource  _resource_for_store.robot
Test Setup  Open browser resurs
Test Teardown  Close all browsers

*** Variables ***
${STYLE-BGC}  background-color: rgb(255, 214, 214);

*** Test Cases ***
Checking if input blank lines
    Click Link  войдите в личный кабинет
    Element Text Should Be  css=h5.auth-title  Вход в интернет-магазин
    Click Element  css=.btn-link.btn-link-blue .btn-link-i
    Sleep  3
    ${AUTH-STYLE}  Get Element Attribute  css=.input-text.auth-input-text@style
    Should Be Equal  ${AUTH-STYLE}  ${STYLE-BGC}
    Sleep  3

Checing if input сorrect password and unregistered email
    Click Link  войдите в личный кабинет
    Element Text Should Be  css=h5.auth-title  Вход в интернет-магазин
    Input Text  name=login  vasyapukinstyle@gmail.com
    Input Password  name=password  ${PASS}
    Sleep  2
    Click Element  css=.btn-link.btn-link-blue .btn-link-i
    Sleep  2
    Page Should Contain Element  css=.auth-message
    Sleep  3

Checing if input сorrect email and wrong password
    Click Link  войдите в личный кабинет
    Element Text Should Be  css=h5.auth-title  Вход в интернет-магазин
    Input Text  name=login  ${LOGIN}
    Input Password  name=password  123456
    Sleep  2
    Click Element  css=.btn-link.btn-link-blue .btn-link-i
    Sleep  2
    Page Should Contain Element  css=.auth-forgot-pass-tooltip-title
    Sleep  3

Checing if input сorrect password and not valid email
    Click Link  войдите в личный кабинет
    Element Text Should Be  css=h5.auth-title  Вход в интернет-магазин
    Input Text  name=login  dima.testqagmail.com
    Input Password  name=password  ${PASS}
    Sleep  2
    Click Element  css=.btn-link.btn-link-blue .btn-link-i
    Sleep  2
    ${AUTH-STYLE}  Get Element Attribute  css=.input-text.auth-input-text@style
    Should Be Equal  ${AUTH-STYLE}  ${STYLE-BGC}
    Clear Element Text  name=login
    Input Text  name=login  dima.testqa@gmailcom
    Sleep  2
    Click Element  css=.btn-link.btn-link-blue .btn-link-i
    Sleep  2
    ${AUTH-STYLE}  Get Element Attribute  css=.input-text.auth-input-text@style
    Should Be Equal  ${AUTH-STYLE}  ${STYLE-BGC}


