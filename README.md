website: 
http://rozetka.com.ua/

Test Cases:
login_test,
Checking links in header,
Checking links in footer,
Checking social media links in footer,
Checking autosuggest and shopping basket,
Checking when input blank lines,
Checking the next step with blank lines,
Checking when input invalid info,
Checking the next step when input invalid info,
Checking when input blank lines as a user,
Checking when input invalid info as a user,

Resource files:
_resource_for_store.robot,
