#!/usr/bin/env python

import requests
import json

url = 'http://138.68.95.151:8080/rest/survey/v1/%s'

def took_token():
    u = url % 'auth/simple'
    value = {
        'clientName': 'string',
        'email': 'string',
        'facebookId': 'string',
        'flag': 'string',
        'googleId': 'string',
        'id': 0,
        'password': 'string',
        'token': 'string'
    }
    headers = {'Content-type': 'application/json'}
    req = requests.post(u, data=json.dumps(value), headers=headers)
    return req.json()['token']

token = took_token()
headers = {
    'Content-type': 'application/json',
    'token': token
}
print took_token()
