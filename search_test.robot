*** Settings ***
Documentation    Its a test for search of website http://rozetka.com.ua/
Resource  _resource_for_store.robot
Test Setup  Open browser resurs
Test Teardown  Close all browsers

*** Test Cases ***
Checking search on website as a user
    Click Link  войдите в личный кабинет
    signin to cabinet
    Input Text  css=.rz-header-search-input-text.passive  ${SEARCH-KEY}
    Sleep  2
    Click Element  name=rz-search-button
    Element Should be visible  css=#search_result_title_text
    Mouse Over  css=#image_item7262848
    Sleep  3
    Page Should Contain Element  css=.g-i-tile-short-detail
    Mouse Over  css=#image_item13252225
    Sleep  3
    Element Should be visible  css=#block_with_search > div > div:nth-child(3) > div > div > div > div.g-i-tile-i-box-desc > ul
    Click Element  css=#image_item7262848
    Sleep  4
    Checking an attribute of item
    Go Back
    Click Element  css=#image_item13252225
    Sleep  4
    Checking an attribute of item

Checking search in the first categori on website as a user
    Click Link  войдите в личный кабинет
    signin to cabinet
    Sleep  3
    Input Text  css=.rz-header-search-input-text.passive  ${SEARCH-KEY}
    Sleep  2
    Click Element  name=rz-search-category-value
    Sleep  3
    Click Element  css=li.rz-header-search-category-l-i:nth-child(2)
    Click Element  name=rz-search-button
    Element Should be visible  css=#search_result_title_text
    Element Text Should Be  css=li.filter-active-i:nth-child(1) > a:nth-child(1)  Компьютеры и ноутбуки
    Mouse Over  css=#image_item3215833
    Sleep  3
    Page Should Contain Element  css=.g-i-tile-short-detail
    Mouse Over  css=#image_item11925530
    Sleep  3
    Element Should be visible  css=#block_with_search > div > div:nth-child(3) > div > div > div > div > ul
    Click Element  css=#image_item3215833
    Sleep  4
    Checking an attribute of item
    Go Back
    Sleep  2
    Click Element  css=#image_item11925530
    Sleep  4
    Checking an attribute of item

Checking search in the second categori on website as a user
    Click Link  войдите в личный кабинет
    signin to cabinet
    Sleep  3
    Input Text  css=.rz-header-search-input-text.passive  ${SEARCH-KEY}
    Sleep  2
    Click Element  name=rz-search-category-value
    Sleep  3
    Click Element  css=li.rz-header-search-category-l-i:nth-child(3)
    Click Element  name=rz-search-button
    Element Should be visible  css=#search_result_title_text
    Element Text Should Be  css=li.filter-active-i:nth-child(1) > a:nth-child(1)  Бытовая техника, интерьер
    Mouse Over  css=#image_item13991066
    Sleep  3
    Page Should Contain Element  css=.g-i-tile-short-detail
    Mouse Over  css=#image_item9797323
    Sleep  3
    Element Should be visible  css=#block_with_search > div > div:nth-child(3) > div > div > div > div > ul
    Click Element  css=#image_item13991066
    Sleep  4
    Checking an attribute of item
    Go Back
    Sleep  2
    Click Element  css=#image_item9797323
    Sleep  4
    Checking an attribute of item