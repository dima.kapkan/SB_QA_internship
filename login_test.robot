*** Settings ***
Documentation    Its a test for login in to website http://rozetka.com.ua/
Resource  _resource_for_store.robot
Test Setup  Open browser resurs
Test Teardown  Close all browsers

*** Test Cases ***
login_test
    Location Should Be  ${HOST}
    Element Should Be Visible  css=.main-big-promo
    Click Link  войдите в личный кабинет
    Element Text Should Be  css=h5.auth-title  Вход в интернет-магазин
    Signin to cabinet
    Wait Until Page Contains Element  css=.logo-owox-link
    Sleep  5
    Click Link  css=#user_menu_container #user_menu #header_user_menu_parent a
    Element Text Should Be  css=.profile-f-i-field  Дмитрий