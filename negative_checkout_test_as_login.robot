*** Settings ***
Documentation  This is a test for cheking negative login test of website http://rozetka.com.ua/
Library  Selenium2Library
Resource  _resource_for_store.robot
Test Setup  Open browser resurs
Test Teardown  Close all browsers

*** Variables ***
${CHECK-INPUT-TEXT}  background-color: rgb(255, 214, 214);

*** Test Cases ***
Checking when input blank lines as a user
    Try to buy a good
    Click Link  Я постоянный клиент
    Signin to cabinet in basket
    Sleep  2
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque.disabled
    Sleep  2
    ${RECIEVER-PHONE}  Get Element Attribute  css=#reciever_phone@style
    Should Be Equal  ${RECIEVER-PHONE}  ${CHECK-INPUT-TEXT}
    Clear Element Text  css=#reciever_name
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque.disabled
    Sleep  2
    ${RECIEVER-NAME}  Get Element Attribute  css=#reciever_name@style
    Should Be Equal  ${RECIEVER-NAME}  ${CHECK-INPUT-TEXT}
    Sleep  3
    Click Element  css=#tip_for_input
    Sleep  2
    Click Element  name=6
    Sleep  2
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque.disabled
    Sleep  2
    ${CHECK-SUGGEST}  Get Element Attribute  css=.input-text.check-suggest-input-text.check-input-text@style
    Should Be Equal  ${CHECK-SUGGEST}  ${CHECK-INPUT-TEXT}

Checking when input invalid info as a user
    Try to buy a good
    Click Link  Я постоянный клиент
    Signin to cabinet in basket
    Sleep  3
    Clear Element Text  css=#reciever_name
    Input Text  css=#reciever_name  !LдыІї]
    Input Text  css=#reciever_phone  @-1231
    Click Element  css=#suggest_locality
    Sleep  2
    Click Element  name=6
    Sleep  2
    Input Text  css=#suggest_locality  @#3111aaaa
    Sleep  2
    Page Should Contain Element  css=.suggestion-i.not-found
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque.disabled
    Sleep  3
    ${CHECK-NAME}  Get Element Attribute  css=#reciever_name@style
    Should Be Equal  ${CHECK-NAME}  ${CHECK-INPUT-TEXT}
    ${CHECK-PHONE}  Get Element Attribute  css=#reciever_phone@style
    Should Be Equal  ${CHECK-PHONE}  ${CHECK-INPUT-TEXT}