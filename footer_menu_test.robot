*** Settings ***
Documentation  This is a test for cheking a footer of website http://rozetka.com.ua
Library  Selenium2Library
Resource  _resource_for_store.robot
Test Setup  Open browser resurs
Test Teardown  Close all browsers

*** Test Cases ***
Checking links in footer
    Click Link  График работы Call-центра
    Wait Until Element Is Visible  css=.title-page h1  7
    Element Text Should Be  css=.title-page h1  Консультации и заказ по телефонам
    Click Link  Подарочные сертификаты
    Wait Until Element Is Visible  css=.c-cols-r-static h1  7
    Element Text Should Be  css=.c-cols-r-static h1  Подарочные сертификаты
    Click Link  Сервисные центры
    Wait Until Element Is Visible  css=.services-title  7
    Element Text Should Be  css=.services-title  Сервисные центры
    Click Link  Вопросы и ответы
    Wait Until Element Is Visible  css=.title-page h1  7
    Element Text Should Be  css=.title-page h1  Вопросы и ответы
    Click Element  css=.footer-l-i #problem_with_order
    Wait Until Element Is Visible  css=.feedback-title  7
    Page Should Contain Element  css=.feedback-title
    Click Element  css=.popup-close
    Click Element  css=.footer-l-i #feedback
    Wait Until Element Is Visible  css=.feedback-title  7
    Page Should Contain Element  css=.feedback-title
    Click Element  css=.popup-close
    Click Element  xpath=//*[@id="body-footer"]/div/div/div[1]/div[3]/div/ul/li[2]/a
    Wait Until Element Is Visible  css=.title-page h1   7
    Element Text Should Be  css=.title-page h1  Консультации и заказ по телефонам
    Click Link  Сотрудничество с нами
    Wait Until Element Is Visible  css=.c-cols-r-static h1   7
    Element Text Should Be  css=.c-cols-r-static h1  Как стать новым партнером компании «Розетка»
    Click Link  Вакансии
    Wait Until Element Is Visible  css=.title-page h1   7
    Element Text Should Be  css=.title-page h1  Построй успешную карьеру вместе с «Розеткой»
    Click Link  Условия использования сайта
    Wait Until Element Is Visible  css=.c-cols-r-static h1   7
    Element Text Should Be  css=.c-cols-r-static h1  Условия использования сайта
    Click Link  Перевод денег с карты на карту
    Wait Until Element Is Visible   css=.text-page   7
    Page Should Contain Element  css=.text-page
    Click Link  Продавать на Розетке
    Page Should Contain  Преимущества для продавцов
    Click Link  Возврат товара
    Signin to cabinet
    Click Link  Возврат товара
    Wait Until Element Is Visible  css=.col-main-wrap-reverse  7
    Page Should Contain  Возврат товара
    Click Link  О нас
    Element Text Should Be  css=.title-page h1  О нас
    Wait Until Element Is Visible  css=.about-map-container  7



