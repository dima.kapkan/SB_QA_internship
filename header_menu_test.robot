*** Settings ***
Documentation  This is a test for cheking a heder of website http://rozetka.com.ua
Library  Selenium2Library
Resource  _resource_for_store.robot
Test Setup  Open browser resurs
Test Teardown  Close all browsers

*** Test Cases ***
Checking links in header
    Click Element  css=.m-top li:nth-of-type(1) a
    Element Text Should Be  css=.title-page h1  Вопросы и ответы
    Click Element  css=.c-cols-l-static .m-static li:nth-of-type(2) a
    Element Text Should Be  css=.credit-head h1  Покупка в кредит
    Click Element  css=.c-cols-l-static .m-static li:nth-of-type(3) a
    Element Text Should Be  css=#deliveries-payments-container h1  Доставка и оплата
    Click Element  css=.c-cols-l-static .m-static li:nth-of-type(4) a
    Element Text Should Be  css=.title-page h1  Гарантия и сервисное обслуживание
    Click Element  css=.c-cols-l-static .m-static li:nth-of-type(5) a
    Element Text Should Be  css=.title-page h1  Консультации и заказ по телефонам
    Click Link  Продавать на Розетке
    Page Should Contain  Преимущества для продавцов
    Click Element  css=#status_orders
    Signin to cabinet
    Page Should Contain  Мои заказы