*** Settings ***
Documentation  This is a test for cheking autosuggest and shopping basket of website http://rozetka.com.ua/
Library  Selenium2Library
Library  BuiltIn
Resource  _resource_for_store.robot
Test Setup  Open browser resurs
#Test Teardown  Close all browsers

*** Test Cases ***
Checking autosuggest and shopping basket
    Click Link  войдите в личный кабинет
    Signin to cabinet
    Wait Until Element Is Visible  css=.rz-header-search-input-text.passive
    Input Text  css=.rz-header-search-input-text.passive  ${AUTOSUGGEST}
    Wait Until Element Is Visible  name=0
    Click Element  name=0
    Checking an attribute of item
    Page Should Contain  DeepCool GAMMAXX 300
    ${PRICE-1}  Get Text  css=#price_label
    ${PRICE-1-INT}   Convert To Integer  ${PRICE-1}
    Element Should Be Visible  css=.sprite-side.btn-link.btn-link-green.detail-buy-btn-link  7
    Click Element  css=.sprite-side.btn-link.btn-link-green.detail-buy-btn-link
    Wait Until Element Is Visible  css=.cart-total-uah span
    ${TOTAL-PRICE}  Get Text  css=.cart-total-uah span
    Convert To Integer  ${TOTAL-PRICE}
    Should Be Equal  ${PRICE-1}  ${TOTAL-PRICE}
    Wait Until Element Is Visible  css=.btn-link.btn-link-gray
    Click Element  css=.btn-link.btn-link-gray
    Input Text  css=.rz-header-search-input-text.passive  ${AUTOSUGGEST}
    Wait Until Element Is Visible  name=2
    Click Element  name=2
    Checking an attribute of item
    Page Should Contain  DeepCool GAMMAXX 400
    Element Should Be Visible  css=.sprite-side.btn-link.btn-link-green.detail-buy-btn-link  7
    Click Element  css=.sprite-side.btn-link.btn-link-green.detail-buy-btn-link
    Wait Until Element Is Visible  name=plus
    Click Link  name=plus
    Checking price in basket
    Wait Until Element Is Visible  name=minus
    Click Link  name=minus
    Checking price in basket
    ${QUANTITY-1}  Get Value  css=.input-text.cart-amount-input-text
    ${QUANTITY-2}  Get Value  xpath=//*[@id="cart-popup"]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[3]/input
    ${QUANTITY-TOTAL}  Evaluate  ${QUANTITY-1} + ${QUANTITY-2}
    ${QUANTITY-ALL}  Convert To Integer  2
    Should be equal  ${QUANTITY-TOTAL}  ${QUANTITY-ALL}
    Page Should Contain Button  css=#popup-checkout
    Wait Until Element Is Visible  css=#popup-checkout
    Click Button  css=#popup-checkout
    Page Should Contain  Оформление заказа
    Input Text  css=#reciever_phone  0682005060
    Wait Until Element Is Visible  css=.btn-link.btn-link-green.check-step-btn-link.opaque
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque
    Wait Until Element Is Visible  name=edit_step
    Click Link  name=edit_step
    Click Link  Редактировать заказ
    Sleep  4
    Delete from basket
    Sleep  3
    Delete from basket
    Sleep  3
    Click Link  name=close
    Element Should Be Visible  css=#profile_signout  10
    Click Link  css=#profile_signout



