#!/usr/bin/env python

import requests
import unittest
import json
import _resource_api_survey


class SurveyTests(unittest.TestCase):


    def test_survey_login(self):
        url = _resource_api_survey.url % 'auth/simple'
        value = {
                'clientName': 'string',
                'email': 'string',
                'facebookId': 'string',
                'flag': 'string',
                'googleId': 'string',
                'id': 0,
                'password': 'string',
                'token': 'string'
               }
        headers = {'Content-type': 'application/json'}
        req = requests.post(url, data=json.dumps(value), headers=headers)
        self.assertEquals(req.status_code, 200)
        print '[PASSED] Authorization is successful'


    def test_get_clients_participant_by_client_id(self):
        url = _resource_api_survey.url % 'participant/all/0'
        req = requests.get(url, headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 200)
        print '[PASSED] Client information is taken'


    def test_update_survey_instanse(self):
        url = _resource_api_survey.url % 'survey'
        value = {
            'clientId': 0,
            'finishTime': '22.04.2017',
            'id': 0,
            'startTime': '22.04.2017',
            'surveyName': 'string',
            'surveyTheme': 'string'
        }
        req = requests.post(url, data=json.dumps(value), headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 201)
        print '[PASSED] Update survey instance is successful'


    def test_create_new_attribute(self):
        url = _resource_api_survey.url % 'attribute'
        value = {
            'attribute': 'string',
            'groupId': 0,
            'id': 0
        }
        req = requests.post(url, data=json.dumps(value), headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 201)
        print '[PASSED] Create new attribute instance'


    def test_get_attributes_by_group_id(self):
        url = _resource_api_survey.url % 'attribute/group/0'
        req = requests.get(url, headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 200)
        print '[PASSED] Get all attributes by group id'


    def test_update_attribute_instance(self):
        url = _resource_api_survey.url % 'attribute/0'
        value = {
            'attribute': 'string',
            'groupId': 0,
            'id': 0
        }
        req = requests.put(url, data=json.dumps(value), headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 200)
        print '[PASSED] Update attribute instance'


    def test_delete_attribute_instanse_by_attribute_id(self):
        url = _resource_api_survey.url % 'attribute/1'
        req = requests.delete(url, headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 200)
        print '[PASSED] Delete attribute instance by attribute id'


    def test_create_new_attribute_value_instanse(self):
        url = _resource_api_survey.url % 'attribute_value'
        value = {
            'attributeId': 0,
            'id': 0,
            'participantId': 0,
            'value': 'string'
        }
        req = requests.post(url, data=json.dumps(value), headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 201)
        print '[PASSED] Create new attribute value instance by attribute id, participant id, value'


    def test_get_attribute_value(self):
        url = _resource_api_survey.url % 'attribute_value/3'
        req = requests.get(url, headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 200)
        print '[PASSED] Get attribute value instance by attribute value id'


    def test_update_attribute_value(self):
        url = _resource_api_survey.url % 'attribute_value/3'
        value = {
            'attributeId': 0,
            'id': 0,
            'participantId': 0,
            'value': 'testprobatestproba'
        }
        req = requests.put(url, data=json.dumps(value), headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 200)
        print '[PASSED] Update attribute value instance by attribute value id'


    def test_create_new_survey_instanse(self):
        url = _resource_api_survey.url % 'survey'
        value = {
            'clientId': 0,
            'finishTime': '05.05.2017',
            'id': 0,
            'startTime': '03.05.2017',
            'surveyName': 'test',
            'surveyTheme': 'test'
        }
        req = requests.post(url, data=json.dumps(value), headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 201)
        print '[PASSED] Create new survey instance by client id, name, theme, start time, finish time'


    def test_delete_survey_from_data_base_by_survey_id(self):
        url = _resource_api_survey.url % 'survey/0'
        req = requests.delete(url, headers=_resource_api_survey.headers)
        self.assertEquals(req.status_code, 200)
        print '[PASSED] Delete survey from data base by survey id'


if __name__ == '__main__':
    unittest.main()
