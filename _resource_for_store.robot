*** Settings ***
Library  Selenium2Library

*** Variables ***
${HOST}  http://rozetka.com.ua/
${LOGIN}  dima.testqa@gmail.com
${PASS}  123456D
${BROWSER}  gc
${AUTOSUGGEST}  DeepCool GAMMAXX
${SEARCH-KEY}  Samsung Galaxy

*** Keywords ***
Signin to cabinet
    Sleep  2
    Input Text  name=login  ${LOGIN}
    Sleep  2
    Input Password  name=password  ${PASS}
    Sleep  2
    Click Element  css=.btn-link.btn-link-blue .btn-link-i
    Sleep  2

Signin to cabinet in basket
    Input Text  css=.input-text.check-auth-input-text.check-input-text  ${LOGIN}
    Input Password  name=user[password]  ${PASS}
    Click Element  css=#signin

Open browser resurs
    Open Browser  ${HOST}  ${BROWSER}
    Maximize Browser Window

Checking price in basket
    Wait Until Element Is Visible  css=.cart-uah.cart-sum-uah span:nth-of-type(1)
    ${PRICE-2}  Get Text  css=.cart-uah.cart-sum-uah span:nth-of-type(1)
    ${PRICE-2-INT}  Convert To Integer  ${PRICE-2}
    ${P-1}  Get Text  xpath=//*[@id="cart-popup"]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/span/span[1]
    ${P-1-INT}  Convert To Integer  ${P-1}
    ${TOTAL-PRICE-ALL}  Get Text  css=.cart-total-uah span:nth-of-type(1)
    ${TOTAL-PRICE-ALL-INT}  Convert To Integer  ${TOTAL-PRICE-ALL}
    ${PRICE-TOTAL}  Evaluate  ${P-1-INT} + ${PRICE-2-INT}
    Should Be Equal  ${PRICE-TOTAL}  ${TOTAL-PRICE-ALL-INT}

Delete from basket
    Wait Until Element Is Visible  name=before_delete
    Click Link  name=before_delete
    Wait Until Element Is Visible  name=delete
    Click Link  name=delete

Try to buy a good
    Input Text  css=.rz-header-search-input-text.passive  Блок питания Aerocool KCAS-700 700W (4713105953282)
    Sleep  2
    Click Element  name=0
    Sleep  3
    Page Should Contain Element  css=.responsive-img
    Page Should Contain Element  css=#price_label
    Element Should Be Visible  css=.sprite-side.btn-link.btn-link-green.detail-buy-btn-link  7
    Click Element  css=.sprite-side.btn-link.btn-link-green.detail-buy-btn-link
    Sleep  4
    Page Should Contain Button  css=#popup-checkout
    Click Button  css=#popup-checkout
    Sleep  4
    Page Should Contain  Оформление заказа

Checking an attribute of item
    Wait Until Element Is Visible  css=.responsive-img
    Page Should Contain Element  css=.responsive-img
    Wait Until Element Is Visible  css=#price_label
    Page Should Contain Element  css=#price_label
