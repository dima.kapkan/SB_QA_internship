*** Settings ***
Documentation  This is a test for cheking negative login test of website http://rozetka.com.ua/
Library  Selenium2Library
Resource  _resource_for_store.robot
Test Setup  Open browser resurs
Test Teardown  Close all browsers

*** Variables ***
${CHECK-INPUT-TEXT}  background-color: rgb(255, 214, 214);
${CHECK-INPUT-BASKET}  color: rgb(128, 128, 128); background-color: rgb(255, 214, 214);

*** Test Cases ***
Checking when input blank lines
    Try to buy a good
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque.disabled
    Sleep  4
    ${CHECK-INPUT}  Get Element Attribute  css=.input-text.check-input-text@style
    Should Be Equal  ${CHECK-INPUT}  ${CHECK-INPUT-TEXT}
    ${CHECK-PHONE}  Get Element Attribute  css=.input-text.check-phone-input-full-text.check-input-text@style
    Should Be Equal  ${CHECK-PHONE}  ${CHECK-INPUT-TEXT}
    Sleep  3
    Click Element  css=#tip_for_input
    Sleep  2
    Click Element  name=6
    Sleep  2
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque.disabled
    Sleep  3
    ${CHECK-SUGGEST}  Get Element Attribute  css=.input-text.check-suggest-input-text.check-input-text@style
    Should Be Equal  ${CHECK-SUGGEST}  ${CHECK-INPUT-TEXT}

Checking the next step with blank lines
    Try to buy a good
    Input Text  css=.input-text.check-input-text  Дима
    Input Text  css=#reciever_phone  0682003030
    Input Text  css=#reciever_email  pupkin@gmailcom.ua
    sleep  5
    element should be visible  css=.btn-link.btn-link-green.check-step-btn-link.opaque
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque
    Sleep  5
    Click Element  name=couriers_5
    Sleep  4
    Click Element  css=#make-order
    sleep  4
    ${RECIEVER-STREET}  Get Element Attribute  css=#reciever_street@style
    Should Be Equal  ${RECIEVER-STREET}  ${CHECK-INPUT-TEXT}
    ${RECIEVER-HOUSE}  Get Element Attribute  css=#reciever_house@style
    Should Be Equal  ${RECIEVER-HOUSE}  ${CHECK-INPUT-TEXT}
    Click Element  name=no_cash
    Sleep  4
    Clear Element Text  name=order[1][additional_fields][recipient_title]
    sleep  2
    Click Element  css=#make-order
    sleep  4
    ${RECIEVER-NAME}  Get Element Attribute  name=order[1][additional_fields][recipient_title]@style
    Should Be Equal  ${RECIEVER-NAME}  ${CHECK-INPUT-TEXT}
    Click Element  name=credit
    Sleep  4
    Click Element  css=#make-order
    Sleep  7
    ${LAST-NAME}  Get Element Attribute  name=order[1][additional_fields][last_name]@style
    Should Be Equal  ${LAST-NAME}  ${CHECK-INPUT-TEXT}
    ${FIRST-NAME}  Get Element Attribute  name=order[1][additional_fields][first_name]@style
    Should Be Equal  ${FIRST-NAME}  ${CHECK-INPUT-TEXT}
    ${SECOND-NAME}  Get Element Attribute  name=order[1][additional_fields][second_name]@style
    Should Be Equal  ${SECOND-NAME}  ${CHECK-INPUT-TEXT}
    ${IND-CODE}  Get Element Attribute  name=order[1][additional_fields][identification_code]@style
    Should Be Equal  ${IND-CODE}  ${CHECK-INPUT-TEXT}
    ${DAY}  Get Element Attribute  name=order[1][additional_fields][birthday_for_credit][day]@style
    Should Be Equal  ${DAY}  ${CHECK-INPUT-BASKET}
    ${MONTH}  Get Element Attribute  name=order[1][additional_fields][birthday_for_credit][month]@style
    Should Be Equal  ${MONTH}  ${CHECK-INPUT-TEXT}
    ${YEAR}  Get Element Attribute  name=order[1][additional_fields][birthday_for_credit][year]@style
    Should Be Equal  ${YEAR}  ${CHECK-INPUT-BASKET}
    ${PASPORT-SER}  Get Element Attribute  name=order[1][additional_fields][passport_number][series]@style
    Should Be Equal  ${PASPORT-SER}  ${CHECK-INPUT-TEXT}
    ${PASPORT-NUMB}  Get Element Attribute  name=order[1][additional_fields][passport_number][number]@style
    Should Be Equal  ${PASPORT-NUMB}  ${CHECK-INPUT-TEXT}
    Click Element  name=card
    Sleep  4
    Click Link  name=visa_change_mail
    Sleep  3
    Clear element text  css=#visa_email_input
    Sleep  2
    Click Element  css=#visa_popup_ok_button
    Sleep  4
    Page Should Contain Element  css=#visa_user_pattern_tooltip
    ${VISA-NAME}  Get Element Attribute  css=#visa_email_input@style
    Should Be Equal  ${VISA-NAME}  ${CHECK-INPUT-TEXT}
    Click Link  name=close
    Sleep  5
    Click Element  css=#make-order
    Sleep  5
    ${LAST-NAME}  Get Element Attribute  name=order[1][additional_fields][last_name]@style
    Should Be Equal  ${LAST-NAME}  ${CHECK-INPUT-TEXT}
    ${FIRST-NAME}  Get Element Attribute  name=order[1][additional_fields][first_name]@style
    Should Be Equal  ${FIRST-NAME}  ${CHECK-INPUT-TEXT}
    ${SECOND-NAME}  Get Element Attribute  name=order[1][additional_fields][second_name]@style
    Should Be Equal  ${SECOND-NAME}  ${CHECK-INPUT-TEXT}

Checking when input invalid info
    Try to buy a good
    Input Text  css=.input-text.check-input-text  !дима
    Input Text  css=#reciever_phone  1234556789
    Input Text  css=#reciever_email  pupkincom.ua
    Click Element  css=#suggest_locality
    Sleep  2
    Click Element  name=6
    Sleep  2
    Input Text  css=#suggest_locality  @#3111aaaa
    Sleep  2
    Page Should Contain Element  css=.suggestion-i.not-found
    Sleep  2
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque.disabled
    Sleep  3
    ${CHECK-INPUT}  Get Element Attribute  css=.input-text.check-input-text@style
    Should Be Equal  ${CHECK-INPUT}  ${CHECK-INPUT-TEXT}
    ${CHECK-PHONE}  Get Element Attribute  css=.input-text.check-phone-input-full-text.check-input-text@style
    Should Be Equal  ${CHECK-PHONE}  ${CHECK-INPUT-TEXT}
    ${CHECK-EMAIL}  Get Element Attribute  css=#reciever_email@style
    Should Be Equal  ${CHECK-EMAIL}  ${CHECK-INPUT-TEXT}

Checking the next step when input invalid info
    Try to buy a good
    Input Text  css=.input-text.check-input-text  Дима
    Input Text  css=#reciever_phone  0682003030
    Input Text  css=#reciever_email  pupkin@gmailcom.ua
    sleep  5
    element should be visible  css=.btn-link.btn-link-green.check-step-btn-link.opaque
    Click Element  css=.btn-link.btn-link-green.check-step-btn-link.opaque
    Sleep  5
    Click link  Изменить город
    Sleep  4
    Clear Element Text  css=#popup_suggest_locality
    Input Text  css=#popup_suggest_locality  !@sda!
    Sleep  3
    Page Should Contain Element  css=.suggestion-i.not-found
    Click Link  name=close
    Click Element  css=.checkbox-label.sprite-side.rz-i-label
    Sleep  5
    Click Element  css=#callback_off_popup .popup-close
    Input Text  name=order[1][additional_fields][last_name]  !@#$%
    Input Text  name=order[1][additional_fields][first_name]  !@#$%
    Input Text  name=order[1][additional_fields][second_name]  !@#$%
    Click Element  css=#make-order
    Sleep  5
    ${LAST-NAME}  Get Element Attribute  name=order[1][additional_fields][last_name]@style
    Should Be Equal  ${LAST-NAME}  ${CHECK-INPUT-TEXT}
    ${FIRST-NAME}  Get Element Attribute  name=order[1][additional_fields][first_name]@style
    Should Be Equal  ${FIRST-NAME}  ${CHECK-INPUT-TEXT}
    ${SECOND-NAME}  Get Element Attribute  name=order[1][additional_fields][second_name]@style
    Should Be Equal  ${SECOND-NAME}  ${CHECK-INPUT-TEXT}
    Click Element  name=no_cash
    Sleep  4
    Clear Element Text  name=order[1][additional_fields][recipient_title]
    Input Text  name=order[1][additional_fields][recipient_title]  !@#$%
    Sleep  2
    Click Element  css=#make-order
    Sleep  5
    ${RECIEVER-NAME}  Get Element Attribute  name=order[1][additional_fields][recipient_title]@style
    Should Be Equal  ${RECIEVER-NAME}  ${CHECK-INPUT-TEXT}