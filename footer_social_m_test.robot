*** Settings ***
Documentation  This is a test for cheking social media links in footer of website http://rozetka.com.ua/
Library  Selenium2Library
Resource  _resource_for_store.robot
Test Setup  Open browser resurs
Test Teardown  Close all browsers

*** Test Cases ***
Checking social media links in footer
     Click Link  css=.social-icon-large-vk.sprite-side
     Sleep  5
     Select Window  title=ROZETKA.UA | ВКонтакте
     Element Text Should Be  css=.page_top h2  ROZETKA.UA
     Close Window
     Select Window  Интернет-магазин ROZETKA™: фототехника, видеотехника, аудиотехника, компьютеры и компьютерные комплектующие
     Click Link  css=.social-icon-large-fb.sprite-side
     Sleep  5
     Select Window  title=Rozetka.ua | Facebook
     Page Should Contain  Facebook
     Close Window
     Select Window  Интернет-магазин ROZETKA™: фототехника, видеотехника, аудиотехника, компьютеры и компьютерные комплектующие
     Click Link  css=.social-icon-large-ok.sprite-side
     Sleep  5
     Select Window  title=Rozetka.ua | OK.RU
     Page Should Contain Element  css=.toolbar_decor
     Close Window
     Select Window  Интернет-магазин ROZETKA™: фототехника, видеотехника, аудиотехника, компьютеры и компьютерные комплектующие
     Click Link  css=.social-icon-large-tw.sprite-side
     Sleep  5
     Select Window  title=Rozetka.ua (@rozetka_news) | Твиттер
     Page Should Contain Element  css=.ProfileCanopy-headerBg
     Close Window
     Select Window  Интернет-магазин ROZETKA™: фототехника, видеотехника, аудиотехника, компьютеры и компьютерные комплектующие
     Click Link  css=.social-icon-large-gp.sprite-side
     Sleep  5
     Select Window  title=Rozetka.ua - Google+
     Page Should Contain Element  css=.Qc4aoc
     Close Window
     Select Window  Интернет-магазин ROZETKA™: фототехника, видеотехника, аудиотехника, компьютеры и компьютерные комплектующие
     Click Link  css=.social-icon-large-yt.sprite-side
     Sleep  5
     Select Window  title=Rozetka.ua - YouTube
     Wait Until Element Is Visible  css=.yt-dialog-fg.yt-uix-overlay-default  7
     Close Window
     Select Window  Интернет-магазин ROZETKA™: фототехника, видеотехника, аудиотехника, компьютеры и компьютерные комплектующие
     Click Link  css=.social-icon-large-in.sprite-side
     Sleep  5
     Select Window  title=@rozetkaua • Фото и видео в Instagram
     Wait Until Element Is Visible  css=._fjpuc._sq03j  7